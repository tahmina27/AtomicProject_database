<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject_database'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

    
    use \App\BITM\SEIP1020\Book\Book;
	use  \App\BITM\SEIP1020\Utility\Utility;

    
    $book = new Book();
    $books = $book->index();
    
    
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:75%;
            }
			
			#message{
                background-color:green;
            }
        </style>
    </head>
    <body>
        <h1>Book Title</h1>
			<div id="message">
				<?php echo Utility::message();?>
			</div>
        <div><span>Search / Filter </span> 
		<span id="utility">Download as PDF | XL  <a href="create.php">Add New</a></span>
		<select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
		</div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
					<th>ID</th>
                    <th>Book Title &dArr;</th>
					<th>Author &dArr;</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
               $slno =1;
               foreach($books as $list){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>
                    <td><?php echo $list->id;?></td>
                    <td><a href="show.php?id=<?php echo $list->id;?>"><?php echo $list->title;?></a></td>
                    <td><?php echo $list->author;?></td>
                    <td><a href="show.php?id=<?php echo $list->id;?>">View</a> 
					| <a href="edit.php?id=<?php echo $list->id;?>">Edit</a>  
					|<a href="delete.php?id=<?php echo $list->id;?>" class="delete"> Delete</a>
                        <!--<form action="delete.php" method="post">
                            <input type="hidden" name ="id" value="<?php echo $list->id;?>">
                        <button type="submit" class="delete">Delete</button>
                        </form>-->
						| Trash/Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
            }
            ?>
                
            </tbody>
        </table>
        <div><span> prev  1 | 2 | 3 next </span></div>
		
		<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(5000);
        </script>
		
    </body>
</html>
