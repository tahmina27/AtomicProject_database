<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
    <!-- Bootstrap -->
    <link href="./Resource/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
	      <div class="container">

        <h1>BITM - Web App Dev - PHP</h1>
        <dl>
            <dt><span>Name:</span></dt>
            <dd>Tahmina Aktar</dd>
            
            <dt>SEIP ID:</dt>
            <dd>107277</dd>
            
            <dt>Batch:</dt>
            <dd>11</dd>
        </dl>
        <h2>Projects</h2>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>01</td>
                    <td><a href="./Views/SEIP1020/Book/index.php">Favourite Books</a></td>
                </tr>
                <tr>
                    <td>02</td>
                    <td><a href="./Views/SEIP1020/Birthday/index.php">Favourite Birthday</a></td>
                </tr>
                <tr>
                    <td>03</td>
                    <td><a href="./Views/SEIP1020/Email/index.php">Favourite Email</a></td>
                </tr>
				<tr>
                    <td>04</td>
                    <td><a href="./Views/SEIP1020/Textarea/index.php">Favourite Email</a></td>
                </tr>
            </tbody>
        </table>
		
		</div>
    </body>
</html>
